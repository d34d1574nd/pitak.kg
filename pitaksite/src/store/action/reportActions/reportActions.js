import axios from "../../../axios-api";
import {NotificationManager} from "react-notifications";

export const createReport = report => {
  return () => {
    return axios.post(`/report/advert/create`, report).then(
      response => {
        NotificationManager.success(response.data.message);
      }
    );
  };
};
