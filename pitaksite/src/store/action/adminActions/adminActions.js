import axios from '../../../axios-api';
import {NotificationManager} from "react-notifications";
import {fetchAdvert, fetchAdverts} from "../advertsActions/advertsActions";
import {push} from "connected-react-router";

export const FETCH_ALL_USERS = 'FETCH_ALL_USERS';
export const LOADING_ALL_USERS_SUCCESS = 'LOADING_ALL_USERS_SUCCESS';

export const fetchAllUsers = users => ({type: FETCH_ALL_USERS, users});
const loadingUsers = cancel => ({type: LOADING_ALL_USERS_SUCCESS, cancel});

export const fetchUsers = () => {
  return dispatch => {
    return axios.get('/admin/users/all').then(
      (response) => {
        dispatch(fetchAllUsers(response.data));
      }
    ).finally(() => dispatch(loadingUsers(false)))
  }
};

export const enabledAdvert = (data) => {
  return dispatch => {
    return axios.post('/admin/adverts/activate', data).then(
      (response) => {
        dispatch(fetchAdverts());
        dispatch(fetchAdvert(data.id_advert));
        NotificationManager.success(response.data.message);
        dispatch(push(`/about_advert/${data.id_advert}`))
      }
    )
  }
};
