import axios from '../../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

export const FETCH_ADVERTS_SUCCESS = 'FETCH_ADVERTS_SUCCESS';
export const FETCH_ADVERT_SUCCESS = 'FETCH_ADVERT_SUCCESS';
export const LOADING_ALL_ADVERTS_SUCCESS = 'LOADING_ALL_ADVERTS_SUCCESS';
export const LOADING_ALL_ADVERT_SUCCESS = 'LOADING_ALL_ADVERT_SUCCESS';

export const fetchAdvertsSuccess = adverts => ({type: FETCH_ADVERTS_SUCCESS, adverts});
export const fetchAdvertSuccess = advert => ({type: FETCH_ADVERT_SUCCESS, advert});
const loadingAdverts = cancel => ({type: LOADING_ALL_ADVERTS_SUCCESS, cancel});
const loadingAdvert = cancel => ({type: LOADING_ALL_ADVERT_SUCCESS, cancel});

export const fetchAdverts = () => {
  return dispatch => {
    return axios.get(`/advert/all`).then(
      response => {
        dispatch(fetchAdvertsSuccess(response.data))
      }
    ).finally(() => dispatch(loadingAdverts(false)));
  };
};

export const fetchAdvert = id => {
  return dispatch => {
    return axios.get(`/advert/about?advertId=${id}`).then(
      response => {
        dispatch(fetchAdvertSuccess(response.data))
      }
    ).finally(() => dispatch(loadingAdvert(false)));
  };
};


export const addAdvertToUser = (data) => {
  return dispatch => {
    return axios.post('/add/advert', data).then(
      (response) => {
        if (response.data.id) {
          NotificationManager.success('Объявление опубликовано');
          dispatch(fetchAdverts());
          dispatch(push(`/about_advert/${response.data.id}`))
        }
      }
    )
  }
};

export const deleteAdvert = (advertId, user) => {
  return dispatch => {
    return axios.delete(`/advert/delete_one/${advertId}`).then(
      response => {
          NotificationManager.success(response.data.message);
          dispatch(fetchAdverts());
          user && user.role === "ROLE_ADMIN" ? dispatch(push(`/admin`)) : dispatch(push(`/`))
      }
    )
  }
};
