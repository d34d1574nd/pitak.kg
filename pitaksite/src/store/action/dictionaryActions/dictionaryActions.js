import axios from '../../../axios-api';

export const FETCH_COUNTRIES_SUCCESS = 'FETCH_COUNTRIES_SUCCESS';
export const FETCH_CITIES_SUCCESS = 'FETCH_CITIES_SUCCESS';
export const FETCH_CAR_BRAND_SUCCESS = 'FETCH_CAR_BRAND_SUCCESS';
export const FETCH_CAR_MODEL_SUCCESS = 'FETCH_CAR_MODEL_SUCCESS';
export const FETCH_CAR_TYPE_SUCCESS = 'FETCH_CAR_TYPE_SUCCESS';
export const FETCH_TYPE_SERVICE_SUCCESS = 'FETCH_TYPE_SERVICE_SUCCESS';

export const fetchCountriesSuccess = countries => ({type: FETCH_COUNTRIES_SUCCESS, countries});
export const fetchCitiesSuccess = cities => ({type: FETCH_CITIES_SUCCESS, cities});
export const fetchCarBrandSuccess = carBrand => ({type: FETCH_CAR_BRAND_SUCCESS, carBrand});
export const fetchCarModelSuccess = carModel => ({type: FETCH_CAR_MODEL_SUCCESS, carModel});
export const fetchCarTypeSuccess = carType => ({type: FETCH_CAR_TYPE_SUCCESS, carType});
export const fetchServiceTypeSuccess = typeService => ({type: FETCH_TYPE_SERVICE_SUCCESS, typeService});

export const fetchCountries = () => {
  return dispatch => {
    return axios.get('/dictionaries/country/get/all').then(
      response => {
        dispatch(fetchCountriesSuccess(response.data))
      }
    );
  };
};

export const fetchCities = parentId => {
  return dispatch => {
    return axios.get(`/dictionaries/city/get/byParent/${parentId}`).then(
      response => {
        dispatch(fetchCitiesSuccess(response.data))
      }
    );
  };
};

export const fetchCarBrand = () => {
  return dispatch => {
    return axios.get(`/dictionaries/carbrand/get/all`).then(
      response => {
        dispatch(fetchCarBrandSuccess(response.data))
      }
    );
  };
};

export const fetchCarModel = carModel => {
  return dispatch => {
    return axios.get(`/dictionaries/carmodel/get/byParent/${carModel}`).then(
      response => {
        dispatch(fetchCarModelSuccess(response.data))
      }
    );
  };
};

export const fetchCarType = () => {
  return dispatch => {
    return axios.get(`/dictionaries/cartype/get/all`).then(
      response => {
        dispatch(fetchCarTypeSuccess(response.data))
      }
    );
  };
};

export const fetchServiceType = () => {
  return dispatch => {
    return axios.get(`/dictionaries/typeService/get/all`).then(
      response => {
        dispatch(fetchServiceTypeSuccess(response.data))
      }
    );
  };
};
