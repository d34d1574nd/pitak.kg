export const FETCH_SEARCH_SUCCESS = 'FETCH_SEARCH_SUCCESS';

export const fetchSearchSuccess = search => ({type: FETCH_SEARCH_SUCCESS, search});

export const fetchSearch = search => {
  return async dispatch => {
      dispatch(await fetchSearchSuccess(search));
  };
};
