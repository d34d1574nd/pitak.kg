import {NotificationManager} from 'react-notifications';
import {push} from 'connected-react-router'

import axios from "../../../axios-api";

export const addCarToUser = (data) => {
  return dispatch => {
    return axios.post('/add/car', data).then(
      (response) => {
        if (response.data) {
          NotificationManager.success('Автомобильно добавлен');
          dispatch(push('/'))
        }
      }
    )
  }
};
