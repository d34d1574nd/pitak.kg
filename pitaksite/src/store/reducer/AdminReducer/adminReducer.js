import {FETCH_ALL_USERS, LOADING_ALL_USERS_SUCCESS} from "../../action/adminActions/adminActions";

const initialState = {
  users: "",
  loading: true,
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_USERS:
      return {...state, users: action.users};
    case LOADING_ALL_USERS_SUCCESS:
      return {...state, loading: action.cancel};
    default:
      return state
  }
};

export default adminReducer;
