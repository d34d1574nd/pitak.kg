import {FETCH_SEARCH_SUCCESS} from "../../action/searchAction/searchAction";

const initialState = {
  search: "",
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SEARCH_SUCCESS:
      return {...state, search: action.search};
    default:
      return state
  }
};

export default searchReducer;
