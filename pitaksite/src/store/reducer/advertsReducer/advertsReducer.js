import {
  FETCH_ADVERT_SUCCESS,
  FETCH_ADVERTS_SUCCESS, LOADING_ALL_ADVERT_SUCCESS, LOADING_ALL_ADVERTS_SUCCESS,

} from "../../action/advertsActions/advertsActions";

const initialState = {
  adverts: "",
  advert: "",
  loading: true,
  loadingAdvert: true
};

const advertsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ADVERTS_SUCCESS:
      return {...state, adverts: action.adverts};
    case FETCH_ADVERT_SUCCESS:
      return {...state, advert: action.advert};
    case LOADING_ALL_ADVERTS_SUCCESS:
      return {...state, loading: action.cancel};
    case LOADING_ALL_ADVERT_SUCCESS:
      return {...state, loadingAdvert: action.cancel};
    default:
      return state
  }
};

export default advertsReducer;
