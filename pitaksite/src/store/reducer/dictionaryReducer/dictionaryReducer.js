import {
  FETCH_CAR_BRAND_SUCCESS, FETCH_CAR_MODEL_SUCCESS, FETCH_CAR_TYPE_SUCCESS,
  FETCH_CITIES_SUCCESS,
  FETCH_COUNTRIES_SUCCESS, FETCH_TYPE_SERVICE_SUCCESS
} from "../../action/dictionaryActions/dictionaryActions";

const initialState = {
  countries: "",
  cities: "",
  carBrand: "",
  carModel: "",
  carType: "",
  typeService: ""
};

const dictionaryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COUNTRIES_SUCCESS:
      return {...state, countries: action.countries};
    case FETCH_CITIES_SUCCESS:
      return {...state, cities: action.cities};
    case FETCH_CAR_BRAND_SUCCESS:
      return {...state, carBrand: action.carBrand};
    case FETCH_CAR_MODEL_SUCCESS:
      return {...state, carModel: action.carModel};
    case FETCH_CAR_TYPE_SUCCESS:
      return {...state, carType: action.carType};
    case FETCH_TYPE_SERVICE_SUCCESS:
      return {...state, typeService: action.typeService};
    default:
      return state
  }
};

export default dictionaryReducer;
