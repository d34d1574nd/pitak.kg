import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const saveState = state => {
  try {
    const serializedState = JSON.stringify(state);
    // localStorage.setItem('state', serializedState);
    sessionStorage.setItem('state', serializedState);
    // cookies.set('token', state.user.user.token);
  } catch (e) {
    console.log('Could not save state');
  }
};

export const loadState = () => {
  try {
    const serializedState = sessionStorage.getItem('state');
    cookies.get('token');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};
