import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import Routes from "./Routes";
import Header from "./components/Header/Header";
import Footer from "./components/footer/Footer";

import {logoutUser} from "./store/action/userActions/userActions";
import SearchComponent from "./components/searchComponent/searchComponent";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Header user={this.props.user} logout={this.props.logoutUser}  />
        <SearchComponent/>
        <NotificationContainer/>
        <Routes user={this.props.user}/>
        <Footer/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
