import React, {Component, Fragment} from 'react';
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import config from "../../../config";
import Logo from "../../../assets/logo.svg";
import Spinner from "../../../components/UI/Spinner/Spinner";
import CardComponent from "../../../components/cardComponent/cardComponent";
import ReactPaginate from "react-paginate";
import {Container} from "@material-ui/core";
import {deleteAdvert, fetchAdverts} from "../../../store/action/advertsActions/advertsActions";
import {connect} from "react-redux";
import Button from "@material-ui/core/Button";

import "./AdvertsPage.css";
import {enabledAdvert} from "../../../store/action/adminActions/adminActions";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

class AdvertsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      data: [],
      perPage: 12,
      currentPage: 0,
      postData: null,
      filters: ""
    };
    this.handlePageClick = this
      .handlePageClick
      .bind(this);
  }

  componentDidMount() {
    this.props.fetchAdverts();
    this.fetchRegionPlacesList();
  };


  componentDidUpdate = async(prevProps, prevState, snapshot) => {
    if (prevProps.adverts !== this.props.adverts) {
      await this.fetchRegionPlacesList();
    }
  };

  fetchRegionPlacesList = async () => {
    // let adverts = await this.filterRange(await this.props.adverts && this.props.adverts, this.state.value[0], this.state.value[1]);
    let adverts = this.props.adverts;
    if(this.state.filters === 'enableTrue') {
      await adverts.sort((a, b) => (b.enabled - a.enabled))
    } else if (this.state.filters === "enableFalse") {
      await adverts.sort((a, b) => (a.enabled - b.enabled))
    }
    const slice = await adverts && adverts.slice(this.state.offset, this.state.offset + this.state.perPage);
    let postDataList = await slice && slice.map(advert => {
      return (
        <div className="advert_one link_about_advert_list">
          <NavLink key={advert && advert.id} className="link_about_advert_list" tag={RouterNavLink} to={`/about_advert/${advert.id}`} >
            <div className="inline_card">
              <img
                className={advert.images ? "img_logo " : "img_inline "}
                src={advert.images ? `${config.apiURL}/uploads/adverts/${advert && advert.images.name}`: Logo}
                alt={advert.images ? advert && advert.images.name : Logo}/>
              <div className="card_inline_text">
                <p className={this.props.loading ? "text_color" : "heading_card"}>{this.props.loading ? "Заголовок" : advert && advert.title}</p>
                <p className={this.props.loading ? "text_color" : null}>{this.props.loading ? "Цена" : advert.username && advert.username.substr(0, 3) === "996" && advert.username.substr(1, 4) ===  "996"  ?
                  advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} тенге`
                  : advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} сом`}</p>
                <p className={this.props.loading ? "text_color" : "title_card_inline"}>{this.props.loading ? "Описание" : advert && advert.text}</p>
              </div>
            </div>
          </NavLink>
          <div className="buttons_admin">
            {advert.enabled === true ?
              <Button >Активное</Button> :
              <Button >Не активное</Button>}
          </div>
        </div>
      );
    });
    this.setState({
      pageCount: Math.ceil( adverts && adverts.length / this.state.perPage),
      postDataList
    })
  };

  handlePageClick = (e) => {
    window.scroll(0,100);
    const selectedPage = e.selected;
    localStorage.setItem('selectedPageAdmin', selectedPage );
    const offset = selectedPage * this.state.perPage;
    this.setState({
      currentPage: selectedPage,
      offset: offset
    }, () => {
      this.fetchRegionPlacesList();
    });
  };

  handleChangeFilters = (event) => {
    this.setState({filters: event.target.value}, () => this.fetchRegionPlacesList());
  };

  render() {
    return (
      <Container>
        <div className={`${this.state.view === 'module' ? `cards` : `inline_cards`}`}>
          <FormControl className='filter_menu_admin'>
            <InputLabel id="filter">Фильтры</InputLabel>
            <Select
              labelId="filter"
              id="filter"
              value={this.state.filters}
              onChange={this.handleChangeFilters}
            >
              <MenuItem value="enableTrue">Сначала активные</MenuItem>
              <MenuItem value="enableFalse">Сначала не активные</MenuItem>
            </Select>
          </FormControl>
          {this.props.loading ? <Fragment>
              <Spinner/>
              <div className="cards_load">
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
              </div>
            </Fragment> :
            <Fragment>

              {this.state.postDataList}
            </Fragment>}
        </div>

        {this.state.postDataList ? <ReactPaginate
          initialPage={Number(localStorage.getItem('selectedPageAdmin')) ? Number(localStorage.getItem('selectedPageAdmin')) : 0}
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={"..."}
          breakClassName="break-me"
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={localStorage.getItem('selectedPageAdmin') !== '' ? 3 : null}
          onPageChange={this.handlePageClick}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName={localStorage.getItem('selectedPageAdmin') !== '' ? "active_pagination" : null} /> : null}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  adverts: state.adverts.adverts,
  user: state.user.user,
  loading: state.adverts.loading,
  search: state.search.search,
});


const mapDispatchToProps = dispatch => ({
  fetchAdverts: () => dispatch(fetchAdverts()),
  enabledAdvert: (data) => dispatch(enabledAdvert(data)),
  deleteAdvert: (advertId, user) => dispatch(deleteAdvert(advertId, user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdvertsPage);
