import React, {Component} from 'react';
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

import {fetchUsers} from "../../store/action/adminActions/adminActions";
import UserList from "./UserList/UserList";
import Spinner from "../../components/UI/Spinner/Spinner";

import "./AdminPage.css";
import CategoryPage from "./CategoryPage/CategoryPage";
import AdvertsPage from "./AdvertsPage/AdvertsPage";

class AdminPage extends Component {
  state = {
    tabIndex: 0
  };

  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <Container>
        <Tabs className="tabs_container_admin" selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({tabIndex})}>
          <TabList>
            <Tab>Список пользователей</Tab>
            <Tab>Список объявлений</Tab>
            <Tab>Список категорий</Tab>
            {/*<Tab>Тип апартамента</Tab>*/}
            {/*<Tab>Тип апартамента</Tab>*/}
          </TabList>
          <TabPanel>
            {this.props.loading ? <Spinner/> : <UserList data={this.props.users}/>}
          </TabPanel>
          <TabPanel>
            {this.props.loading ? <Spinner/> : <AdvertsPage/>}
          </TabPanel>
          <TabPanel>
            {this.props.loading ? <Spinner/> : <CategoryPage/>}
          </TabPanel>
          {/*<TabPanel>*/}
          {/*  dd*/}
          {/*</TabPanel>*/}
          {/*<TabPanel>*/}
          {/*  dd*/}
          {/*</TabPanel>*/}

        </Tabs>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  users: state.admin.users,
  loading: state.admin.loading,
});


const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);
