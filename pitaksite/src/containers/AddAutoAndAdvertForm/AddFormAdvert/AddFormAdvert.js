import React, {Component} from 'react';
import carImage from "../../../assets/carImageDefault.svg";
import {Col, FormGroup, FormText} from "reactstrap";
import {
  fetchCarBrand, fetchCarModel,
  fetchCarType, fetchCities,
  fetchCountries, fetchServiceType
} from "../../../store/action/dictionaryActions/dictionaryActions";
import {connect} from "react-redux";
import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import {NotificationManager} from "react-notifications";

import "./addFormAdvert.css";
import {addAdvertToUser} from "../../../store/action/advertsActions/advertsActions";

class AddFormAdvert extends Component {
  state = {
    amount_payment: 1,
    from_place: "",
    to_place: "",
    title: "",
    text: "",
    send_datetime: "",
    date_from_place: new Date(),
    date_to_place: new Date(),
    time_from_place: new Date(),
    time_to_place: new Date(),
    imgSrc: [],
    advertPhoto: null,
    car_type_id: null,
    car_brand_id: null,
    car_model_id: null,
    carry_capacity: null,
  };

  componentDidMount() {
    this.props.fetchCarType();
    this.props.fetchServiceType();
    this.props.fetchCities(2);
    this.props.fetchCarBrand();
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  ChangeDatePickerFromPlace = date => {
    this.setState({
      date_from_place: date
    })
  };
  ChangeDatePickerToPlace = date => {
    this.setState({
      date_to_place: date
    })
  };
  ChangeTImePickerFromPlace = date => {
    this.setState({
      time_from_place: date
    })
  };
  ChangeTimePickerToPlace = date => {
    this.setState({
      time_to_place: date
    })
  };

  fileChangeHandler = event => {
    const files = event.target.files;
    const fileValidation = /(.*?)\.(jpg|bmp|jpeg|png|img)$/;
    if (event.target.value.match(fileValidation)) {
      for (let i = 0; i < files.length; i++) {
        let reader = new FileReader();
        reader.readAsDataURL(files[i]);
        reader.onloadend = function (e) {
          this.setState({
            imgSrc: [...this.state.imgSrc, reader.result]
          })
        }.bind(this);
        this.setState({
          [event.target.name]: event.target.files
        })
      }
    } else {
      NotificationManager.error('Не поддерживаемый формат файла')
    }
  };

  submitFormHandlerAdvert = event => {
    event.preventDefault();

    const formData = new FormData();
    formData.append("amount_payment",  this.state.amount_payment);
    formData.append("from_place", `${this.state.from_place} ${this.state.date_from_place.toLocaleString('ru-RU', {year: 'numeric', month: 'long', day: 'numeric' })} ${this.state.time_from_place.toLocaleString('ru-RU', {hour: 'numeric', minute: 'numeric' })}` );
    formData.append("to_place", `${this.state.to_place} ${this.state.date_to_place.toLocaleString('ru-RU', {year: 'numeric', month: 'long', day: 'numeric' })} ${this.state.time_to_place.toLocaleString('ru-RU', {hour: 'numeric', minute: 'numeric' })}`);
    formData.append("title", this.state.title);
    formData.append("text", this.state.text);
    formData.append("created_by_id", this.props.user.id);
    formData.append("username", this.props.user.username);
    formData.append("car_type_id", this.state.car_type_id);
    formData.append("car_brand_id", this.state.car_brand_id);
    formData.append("car_model_id", this.state.car_model_id);
    formData.append("user_id", this.props.user.id);
    formData.append("carry_capacity", this.state.carry_capacity);
    for (let i = 0; i < this.state.advertPhoto.length; i++) {
      formData.append("advertPhoto", this.state.advertPhoto[i]);
    }

    this.props.addAdvertToUser(formData);
  };

  render() {
    let type = this.props.carType && this.props.carType.sort((a, b) => {
      return b.id - a.id
    });
    let typesCar = type.slice(1, 21);
    return (
      <form className="form_add_auto" onSubmit={this.submitFormHandlerAdvert}>
        <div className="form_add_auto--input_1">
          <label htmlFor="file-input" className="label_text-img">
            <p>Загрузите фото</p>
            {this.state.advertPhoto !== null ?
              <div className="imgs">
                <img className="car_image" src={this.state.imgSrc[0]} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[1] ? this.state.imgSrc[1] : carImage} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[2] ? this.state.imgSrc[2] : carImage} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[3] ? this.state.imgSrc[3] : carImage} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[4] ? this.state.imgSrc[4] : carImage} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[5] ? this.state.imgSrc[5] : carImage} alt="icon car"/>
                <img className="image_car" src={this.state.imgSrc[6] ? this.state.imgSrc[6] : carImage} alt="icon car"/>
              </div> :
              <div className="imgs">
                <img src={carImage} alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
                <img src={carImage} className="image_car" alt="car"/>
              </div>
            }
          </label>
          <input id="file-input" required={true} type="file" multiple={7} name="advertPhoto" onChange={this.fileChangeHandler}/>
        </div>
        <div className="form_add_advert--inputs">
          <label htmlFor="title">
            Заголовок
          </label>
          <input required={true}  id="title" type="text" name="title" placeholder="Заголовок" value={this.state.title} onChange={this.inputChangeHandler}/>
        </div>
        <div className="form_add_advert--inputs">
          <label htmlFor="text">Описание</label>
          <textarea  required={true} id="text" cols="20" rows="5" name="text" placeholder="Описание" value={this.state.text} onChange={this.inputChangeHandler}/>
        </div>
        <div className="form_add_auto--inputs">
          <label htmlFor="carType">
            Тип Автомобиля
          </label>
          <select id="carType"  name="car_type_id" required={true} onChange={this.inputChangeHandler} value={this.state.car_type_id}>
            <option value="">Тип Автомобиля</option>
            {this.props.carType && typesCar.map(type => (
              <option key={type.id} value={type.id}>{type.name}</option>
            ))}
          </select>
        </div>
        <div className="form_add_auto--inputs">
          <label htmlFor="carBrand">Марка транспорта</label>
          <select id="carBrand"  name="car_brand_id" required={true}  onChange={this.inputChangeHandler}
                  value={this.state.carBrand} onClick={() => this.props.fetchCarModel(this.state.car_brand_id)}
          >
            <option value="">Марка транспорта</option>
            {this.props.carBrand && this.props.carBrand.map(type => (
              <option key={type.id} value={type.id}>{type.name}</option>
            ))}
          </select>
        </div>
        {this.state.car_brand_id ? <div className="form_add_auto--inputs">
          <label htmlFor="carModel">Модель транспорта</label>
          <select id="carModel" name="car_model_id" required={true}  onChange={this.inputChangeHandler} value={this.state.car_model_id}>
            <option value="">Модель транспорта</option>
            {this.props.carModel && this.props.carModel.map(type => (
              <option key={type.id} value={type.id}>{type.name}</option>
            ))}
          </select>
        </div> : null}
        <div className="form_add_auto--inputs">
          <label htmlFor="carryCapacity">Грузоподъёмность (кг)</label>
          <input id="carryCapacity" type="number" required={true}  name="carry_capacity" value={this.state.carry_capacity} onChange={this.inputChangeHandler} placeholder="Грузоподъёмность (кг)"/>
        </div>
        {/*<div className="form_add_advert--inputs">*/}
        {/*  <label htmlFor="countryId">Страна</label>*/}
        {/*  <select id="countryId" name="countryId" value={this.state.countryId} className="registerAuto"*/}
        {/*          onChange={this.inputChangeHandler} onClick={() => this.props.fetchCities(this.state.countryId)}*/}
        {/*  >*/}
        {/*    <option value="">Cтрана</option>*/}
        {/*    {this.props.countries && this.props.countries.map(country => (*/}
        {/*      <option key={country.id} value={country.id}>{country.name}</option>*/}
        {/*    ))}*/}
        {/*  </select>*/}
        {/*</div>*/}
        <div className="form_add_advert--inputs">
          <label htmlFor="from_place">
            Откуда
          </label>
          <div className="inputs_where">
            <select  id="from_place" className="registerAdvert_where" name="from_place" onChange={this.inputChangeHandler} >
              <option value="">Город</option>
              {this.props.cities && this.props.cities.map(city => (
                <option key={city.id} value={city.id}>{city.name}</option>
              ))}
            </select>
            <DatePicker clearIcon={false}
                        className="registerAdvert_date" locale="ru-RU" calendarIcon={false}
              onChange={this.ChangeDatePickerFromPlace} minDate={new Date()}
              value={this.state.date_from_place} name="date_from_place"
            />
            <TimePicker clearIcon={false}
                        className="registerAdvert_time"  locale="ru-RU" clockIcon={false}
              onChange={this.ChangeTImePickerFromPlace}
              value={this.state.time_from_place} name="time_from_place"
            />
          </div>
        </div>
        <div className="form_add_advert--inputs">
          <label htmlFor="to_place">
            Куда
          </label>
          <div className="inputs_where">
            <select  id="to_place" className="registerAdvert_where" name="to_place" onChange={this.inputChangeHandler} >
              <option value="">Город</option>
              {this.props.cities && this.props.cities.map(city => (
                <option key={city.id} value={city.id}>{city.name}</option>
              ))}
            </select>
            <DatePicker clearIcon={false}
                        className="registerAdvert_date" locale="ru-RU" calendarIcon={false}
              onChange={this.ChangeDatePickerToPlace} minDate={new Date()}
              value={this.state.date_to_place} name="date_to_place"
            />
            <TimePicker clearIcon={false}
                        className="registerAdvert_time" locale="ru-RU"
              onChange={this.ChangeTimePickerToPlace} name="time_to_place"
              value={this.state.time_to_place} clockIcon={false}
            />
          </div>
          <FormText color="muted" className="form_text">
            Поля откуда и куда не обязательны к заполнению если они Вам не требуются
          </FormText>
        </div>

        <div className="form_add_advert--inputs">
          <label htmlFor="amount_payment">
            Цена
          </label>
          <div className="price">
            <input type="number" id="amount_payment" className="registerAdvert_price" name="amount_payment" placeholder="Цена" onChange={this.inputChangeHandler}/>
            {/*<select  className="registerAuto registerAdvert" name="currency" onChange={this.inputChangeHandler} value={this.state.currency}>*/}
            {/*  <option value="Сом" disabled>Валюта</option>*/}
            {/*  <option value="Сом">Сом</option>*/}
            {/*  <option value="Тенге">Тенге</option>*/}
            {/*</select>*/}
          </div>
          <FormText color="muted" className="form_text">
            Если цену не указывать она будет автоматически записана как "Договорная"
          </FormText>
        </div>

        <FormGroup check row>
          <Col sm={{ size: 9, offset: 3 }}>
            <button className="add_auto_btn" type="submit">Опубликовать</button>
          </Col>
        </FormGroup>
      </form>
    );
  }
}
const mapStateToProps = state => ({
  carType: state.dictionary.carType,
  countries: state.dictionary.countries,
  cities: state.dictionary.cities,
  typeService: state.dictionary.typeService,
  carBrand: state.dictionary.carBrand,
  carModel: state.dictionary.carModel,
  user: state.user.user,
});


const mapDispatchToProps = dispatch => ({
  fetchCarType: () => dispatch(fetchCarType()),
  fetchServiceType: () => dispatch(fetchServiceType()),
  fetchCountries: () => dispatch(fetchCountries()),
  fetchCarBrand: () => dispatch(fetchCarBrand()),
  fetchCarModel: carModel => dispatch(fetchCarModel(carModel)),
  fetchCities: (parentId) => dispatch(fetchCities(parentId)),
  addAdvertToUser: (data) => dispatch(addAdvertToUser(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddFormAdvert);
