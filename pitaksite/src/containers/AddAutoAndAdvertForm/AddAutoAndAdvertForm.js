import React, {Component} from 'react';
import {Container} from "@material-ui/core";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";

import AddFormAdvert from "./AddFormAdvert/AddFormAdvert";

import "./AddAutoAndAdvertForm.css";

class AddAutoAndAdvertForm extends Component {
  render() {
    return (
      <Container>
        <div className="nav_add">
          <NavLink tag={RouterNavLink} to="/advert">Создать объявление</NavLink>
        </div>
        <AddFormAdvert/>
      </Container>
    );
  }
}

export default AddAutoAndAdvertForm
