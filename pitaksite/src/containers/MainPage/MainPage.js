import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";
import ReactPaginate from 'react-paginate';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import {fetchAdverts} from "../../store/action/advertsActions/advertsActions";
import CardComponent from "../../components/cardComponent/cardComponent";
import Spinner from "../../components/UI/Spinner/Spinner";
import config from "../../config";

import Logo from "../../assets/logo.svg";

import "./MainPage.css";
import "./MediaStyle.css";
import ToggleButtons from "../../components/UI/ToggleButtons/ToggleButtons";
// import SliderComponent from "../../components/SliderComponent/SliderComponent";


class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      data: [],
      perPage: 12,
      currentPage: 0,
      postData: null,
      view: "module",
      value: [0, 3000],
      filters: ""
    };
    this.handlePageClick = this
      .handlePageClick
      .bind(this);
  }

  componentDidMount() {
    this.props.fetchAdverts();
    this.state.view === 'module' ?  this.fetchRegionPlaces() : this.fetchRegionPlacesList();
  };



  componentDidUpdate = async(prevProps, prevState, snapshot) => {
    if (prevProps.adverts !== this.props.adverts) {
      this.state.view !== prevState.view ? await this.fetchRegionPlacesList() : await this.fetchRegionPlaces();
    }
  };

  filterRange = async (arr, a, b) => {
    return await arr.filter(item => (a <= item.amount_payment && item.amount_payment <= b));
  };

  fetchRegionPlaces = async () => {
    // let adverts = await this.filterRange(await this.props.adverts && this.props.adverts, this.state.value[0], this.state.value[1]);
    let adverts = [];
    await this.props.adverts && this.props.adverts.map(advert => {
      if (advert.enabled) {
        adverts = [...adverts, advert]
      }});
    if(this.state.filters === 'priceDescending') {
      await adverts.sort((a, b) => (b.amount_payment - a.amount_payment))
    } else if (this.state.filters === "priceAscending") {
      await adverts.sort((a, b) => (a.amount_payment - b.amount_payment))
    } else if (this.state.filters ===  "fromOldToNew") {
      await adverts.sort((a, b) => (a.id - b.id))
    } else if (this.state.filters ===  "fromNewToOld") {
      await adverts.sort((a, b) => (b.id - a.id))
    }
    const slice = await adverts && adverts.slice(this.state.offset, this.state.offset + this.state.perPage);
    let postData = await slice && slice.map(advert => {
          return (
            <NavLink key={advert && advert.id} className="link_about_advert" tag={RouterNavLink} to={`/about_advert/${advert.id}`} >
              <CardComponent
                name={advert.name ? advert.name.charAt(0).toUpperCase() : null}
                heading={advert && advert.title}
                image={advert.images ? `${config.apiURL}/uploads/adverts/${advert && advert.images.name}` : Logo}
                price={
                  advert.username && advert.username.substr(0, 3) === "996" && advert.username.substr(1, 4) ===  "996"  ?
                    advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} тенге`
                    : advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} сом`
                }
                title={advert && advert.text}
              />
            </NavLink>
          );
    });
    this.setState({
      pageCount: Math.ceil( adverts && adverts.length / this.state.perPage),
      postData
    })
  };

  fetchRegionPlacesList = async () => {
    // let adverts = await this.filterRange(await this.props.adverts && this.props.adverts, this.state.value[0], this.state.value[1]);
    let adverts = [];
    this.props.adverts && this.props.adverts.map(advert => {
      if (advert.enabled) {
        adverts = [...adverts, advert]
      }});
    if(this.state.filters === 'priceDescending') {
      await adverts.sort((a, b) => (b.amount_payment - a.amount_payment))
    } else if (this.state.filters === "priceAscending") {
      await adverts.sort((a, b) => (a.amount_payment - b.amount_payment))
    } else if (this.state.filters ===  "fromOldToNew") {
      await adverts.sort((a, b) => (a.id - b.id))
    } else if (this.state.filters ===  "fromNewToOld") {
      await adverts.sort((a, b) => (b.id - a.id))
    }
    const slice = await adverts && adverts.slice(this.state.offset, this.state.offset + this.state.perPage);
    let postDataList = await slice && slice.map(advert => {
      return (
        <NavLink key={advert && advert.id} className="link_about_advert_list" tag={RouterNavLink} to={`/about_advert/${advert.id}`} >
            <div className="inline_card">
              <img
                className={advert.images ? "img_logo " : "img_inline "}
                src={advert.images ? `${config.apiURL}/uploads/adverts/${advert && advert.images.name}`: Logo}
                alt={advert.images ? advert && advert.images.name : Logo}/>
                <div className="card_inline_text">
                  <p className={this.props.loading ? "text_color" : "heading_card"}>{this.props.loading ? "Заголовок" : advert && advert.title}</p>
                  <p className={this.props.loading ? "text_color" : null}>{this.props.loading ? "Цена" : advert.username && advert.username.substr(0, 3) === "996" && advert.username.substr(1, 4) ===  "996"  ?
                    advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} тенге`
                    : advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} сом`}</p>
                  <p className={this.props.loading ? "text_color" : "title_card_inline"}>{this.props.loading ? "Описание" : advert && advert.text}</p>
                </div>
            </div>
        </NavLink>
      );
    });
    this.setState({
      pageCount: Math.ceil( adverts && adverts.length / this.state.perPage),
      postDataList
    })
  };

  handlePageClick = (e) => {
    window.scroll(0,100);
    const selectedPage = e.selected;
    localStorage.setItem('selectedPage', selectedPage );
    const offset = selectedPage * this.state.perPage;
      this.setState({
        currentPage: selectedPage,
        offset: offset
      }, () => {
        this.state.view === 'module' ? this.fetchRegionPlaces() : this.fetchRegionPlacesList();
      });
  };

  handleChange = (view) => {
    let viewBlock = null;
    view === null ? viewBlock = this.state.view : viewBlock = view;
    this.setState({view: viewBlock}, () => {
      this.state.view === 'module' ? this.fetchRegionPlaces() : this.fetchRegionPlacesList();
    })
  };

  handleRange = value => {
    this.setState({
      value: value
    });
    this.state.view === 'module' ? this.fetchRegionPlaces() : this.fetchRegionPlacesList();
  };

  handleChangeFilters = (event) => {
    this.setState({filters: event.target.value}, () => this.state.view === 'module' ? this.fetchRegionPlaces() : this.fetchRegionPlacesList());
  };

  render() {
    return (
      <Container>
        <div className="heading_title">
          <h3 className="heading">Новые объявления</h3>
          {/*<SliderComponent value={this.state.value} handleRange={this.handleRange} value1={this.state.value[0]} value2={this.state.value[1]}/>*/}
          <FormControl className='filter_menu' >
            <InputLabel id="filter">Фильтры</InputLabel>
            <Select
              labelId="filter"
              id="filter"
              value={this.state.filters}
              onChange={this.handleChangeFilters}
            >
              <MenuItem value="priceDescending">По цене (по убыванию)</MenuItem>
              <MenuItem value="priceAscending">По цене (по возрастанию)</MenuItem>
              <MenuItem value="fromOldToNew">От старого до нового</MenuItem>
              <MenuItem value="fromNewToOld">От нового до старого</MenuItem>
            </Select>
          </FormControl>
          <ToggleButtons view={this.state.view} handleChanges={this.handleChange}/>
        </div>
        <div className={`${this.state.view === 'module' ? `cards` : `inline_cards`}`}>
          {this.props.loading ? <Fragment>
              <Spinner/>
              <div className="cards_load">
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
                <CardComponent loading={this.props.loading}/>
              </div>
          </Fragment> :
            <Fragment>
              {this.state.view === "module" ? this.state.postData : this.state.postDataList}
            </Fragment>}
        </div>

        {this.state.postData ? <ReactPaginate
          initialPage={Number(localStorage.getItem('selectedPage')) ? Number(localStorage.getItem('selectedPage')) : 0}
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={"..."}
          breakClassName="break-me"
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={localStorage.getItem('selectedPage') !== '' ? 3 : null}
          onPageChange={this.handlePageClick}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName={localStorage.getItem('selectedPage') !== '' ? "active_pagination" : null} /> : null}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  adverts: state.adverts.adverts,
  loading: state.adverts.loading,
  search: state.search.search,
});


const mapDispatchToProps = dispatch => ({
  fetchAdverts: () => dispatch(fetchAdverts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
