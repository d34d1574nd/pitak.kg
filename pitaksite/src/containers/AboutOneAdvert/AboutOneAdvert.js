import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";

import Spinner from "../../components/UI/Spinner/Spinner";
import ModalWindowComponent from "../../components/UI/modalWindowComponent/modalWindowComponent";
import ImageSlider from "../../components/imageSlider/imageSlider";

import {deleteAdvert, fetchAdvert} from "../../store/action/advertsActions/advertsActions";
import Logo from "../../assets/logo.svg";

import "./AboutOneAdvert.css";
import "./AboutOneAdvertMedia.css";
import {createReport} from "../../store/action/reportActions/reportActions";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";
import {enabledAdvert} from "../../store/action/adminActions/adminActions";


class AboutOneAdvert extends Component {
  state = {
    number: false,
    modal: false,
    additionalDescription: "",
    advertId: this.props.match.params.advert,
    reportTypeId: []
  };

  componentDidMount() {
    this.props.fetchAdvert(this.props.match.params.advert);
  }

  numberShow = () => {
    this.setState({
      number: !this.state.number
    })
  };

  showModal = () => {
    this.setState({
      modal: !this.state.modal
    })
  };


  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  changeCheckbox = (event) => {
    if (event.target.checked) {
      this.setState({reportTypeId: event.target.value})
    }
  };

  onSubmitForm = () => {
    this.props.createReport({...this.state, user_id: this.props.user.id});
    this.setState({
      modal: !this.state.modal,
    })
  };
  onBackButton = () => {
    this.props.history.goBack()
  };

  render() {
    if (this.props.loading) {
      return <Spinner/>;
    }
    let date = this.props.advert && this.props.advert.send_datetime && new Date(this.props.advert.send_datetime);
    return (
      <Container>
        <div className="advert">
          <div className="advert_body">
            <NavLink tag={RouterNavLink} className="return" to="/">На главную</NavLink>
            <NavLink tag={RouterNavLink} className="return-search" to="#" onClick={this.onBackButton}>Назад</NavLink>

            <h3>{this.props.advert && this.props.advert.title}</h3>
            {this.props.advert && this.props.advert.images ? <ImageSlider loading={this.props.loading} image={this.props.advert && this.props.advert.images}/> :
              <div className="img_div_about">
                <img src={Logo} alt="logo"/>
              </div>}
            {this.props.advert && this.props.advert.car.length === 0 ? null :
              <div className="info">
                <div className="info-1">
                  {this.props.advert && this.props.advert.car.car_type ?
                    <p>Тип услуги <span>{this.props.advert && this.props.advert.car.car_type.name}</span></p> :
                    null}
                  {this.props.advert && this.props.advert.car.car_type.name ?
                    <p>Тип транспорта <span>{this.props.advert && this.props.advert.car.car_type.name}</span></p> :
                    null}
                  {this.props.advert.car && this.props.advert.car.carry_capacity ?
                    <p>Грузоподъемность <span>{this.props.advert && this.props.advert.car.carry_capacity}</span></p> :
                    null}
                </div>
                <div className="info-2">
                  {this.props.advert && this.props.advert.car.car_brand ?
                    <p>Марка транспорта <span>{this.props.advert.car && this.props.advert.car.car_brand.name}</span></p> :
                    null}
                  {this.props.advert && this.props.advert.car.car_model ?
                    <p>Модель транспорта <span>{this.props.advert && this.props.advert.car.car_model.name}</span></p> :
                    null}
                </div>
              </div> }
            <div className="description">
              <p>Описание </p>
              <span>{this.props.advert &&  this.props.advert.text}</span>
            </div>
            {this.props.advert && this.props.advert.send_datetime ?  <div className="times">
              <p>Создано <span>{date.toLocaleString('ru-RU', {year: 'numeric', month: 'long', day: 'numeric' })}</span></p>
              {/*<p>Обновлено <span>12 декабря 2020г.</span></p>*/}
            </div> : null}
          </div>
          <div className="buttons_price">
              <h3>{
                this.props.advert.username ?
                  this.props.advert.username.substr(0, 3) === "996" && this.props.advert.username.substr(1, 4) ===  "996" ?
                    `${this.props.advert && this.props.advert.amount_payment} тенге`
                    : this.props.advert.amount_payment === 1 || this.props.advert.amount_payment === null ? "Договорная" : `${this.props.advert && this.props.advert.amount_payment} сом` :
                  "Договорная"
              }</h3>
            {this.state.number ?  <a href={`tel:${this.props.advert.username !== null ? this.props.advert.username : null}`} className="button btn-1 link_phone">
              {this.props.advert.username !== null ? `+${this.props.advert.username}` : "Водитель не предоставил номер"}
            </a>  : <Button className="button btn-1" variant="contained" color="primary" onClick={this.numberShow}>
              Показать номер
            </Button> }
            {this.props.user && this.props.user.role.name === "ROLE_ADMIN" ?
              <Fragment>
                {this.props.advert.enabled ?
                  <Button className="button btn-3" onClick={() => this.props.enabledAdvert({id_advert: this.props.advert.id, enabled: false})}>Деактивировать</Button> :
                  <Button className="button btn-3" onClick={() => this.props.enabledAdvert({id_advert: this.props.advert.id, enabled: true})}>Активировать</Button>}
                <Button className="button btn-3" variant="outlined" color="secondary" onClick={this.showModal}>
                  Удалить
                </Button>
              </Fragment> : this.props.user ? <Fragment>
                <Button className="button btn-2" variant="outlined" color="primary">
                  Добавить в избранное
                </Button>
                <Button className="button btn-3" variant="outlined" color="secondary" onClick={this.showModal}>
                  Пожаловаться
                </Button>
              </Fragment> : null}
          </div>
        </div>

        {this.state.modal ? <ModalWindowComponent submit={this.onSubmitForm}
          deleteAdvert={() => this.props.deleteAdvert(this.props.advert.id)}
          role={this.props.user.role.name} modal={this.state.modal} handleClose={this.showModal}
          additionalDescription={this.state.additionalDescription} changeCheckbox={this.changeCheckbox}
          reportTypeId={this.state.reportTypeId} inputChangeHandler={this.inputChangeHandler}
        /> : null}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  advert: state.adverts.advert,
  loading: state.adverts.loadingAdvert,
  user: state.user.user,
});


const mapDispatchToProps = dispatch => ({
  fetchAdvert: id => dispatch(fetchAdvert(id)),
  deleteAdvert: advertId => dispatch(deleteAdvert(advertId)),
  enabledAdvert: (data) => dispatch(enabledAdvert(data)),
  createReport: report => dispatch(createReport(report)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AboutOneAdvert);
