import React, {Component} from 'react';
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";
import ReactPaginate from 'react-paginate';

import CardComponent from "../../components/cardComponent/cardComponent";
import config from "../../config";

import Logo from "../../assets/logo.svg";
import noFound from "../../assets/nothing-found.png";
// import SliderComponent from "../../components/SliderComponent/SliderComponent";
import ToggleButtons from "../../components/UI/ToggleButtons/ToggleButtons";

import "./SearchContainer.css"
import "./SearchContainerMedia.css"
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

class SearchContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      data: [],
      perPage: 12,
      currentPage: 0,
      postData: null,
      loading: true,
      value: [0, 3000],
      view: "module",
      filters: ""
    };
    this.handlePageClick = this
      .handlePageClick
      .bind(this);
  }

  componentDidMount = async () => {
    await this.state.view === 'module' ? this.fetchSearchPlaces() : this.fetchSearchPlacesList();
  };



  componentDidUpdate = async(prevProps, prevState, snapshot) => {
    if (prevProps.search !== this.props.search) {
      await this.fetchSearchPlaces();
      await this.fetchSearchPlacesList();
    }
  };

  filterRange = async (arr, a, b) => {
    return await arr.filter(item => (a <= item.amount_payment && item.amount_payment <= b));
  };

  fetchSearchPlaces = async () => {
    // let adverts = await this.filterRange(await this.props.search && this.props.search, this.state.value[0], this.state.value[1]);
    let adverts = [];
    this.props.search && this.props.search.map(advert => {
      if (advert.enabled) {
        adverts = [...adverts, advert]
      }});
    if(this.state.filters === 'priceDescending') {
      await adverts.sort((a, b) => (b.amount_payment - a.amount_payment))
    } else if (this.state.filters === "priceAscending") {
      await adverts.sort((a, b) => (a.amount_payment - b.amount_payment))
    } else if (this.state.filters ===  "fromOldToNew") {
      await adverts.sort((a, b) => (a.id - b.id))
    } else if (this.state.filters ===  "fromNewToOld") {
      await adverts.sort((a, b) => (b.id - a.id))
    }
    const slice = await adverts && adverts.slice(this.state.offset, this.state.offset + this.state.perPage);
    let postData = await slice && slice.map(advert => {
      return (
        <NavLink key={advert && advert.id} className="link_about_advert" tag={RouterNavLink} to={`/about_advert/${advert.id}`} >
          <CardComponent
            name={advert.name ? advert.name.charAt(0).toUpperCase() : null}
            heading={advert && advert.title}
            image={advert.images ? `${config.apiURL}/uploads/adverts/${advert && advert.images.name}` : Logo}
            price={
              advert.username && advert.username.substr(0, 3) === "996" && advert.username.substr(1, 4) ===  "996"  ?
                advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} тенге`
                : advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} сом`
            }
            title={advert && advert.text}
          />
        </NavLink>
      );
    });
    this.setState({
      pageCount: Math.ceil( adverts && adverts.length / this.state.perPage),
      postData
    })
  };

  fetchSearchPlacesList = async () => {
    // let adverts = await this.filterRange(await this.props.search && this.props.search, this.state.value[0], this.state.value[1]);
    let adverts = [];
    this.props.search && this.props.search.map(advert => {
      if (advert.enabled) {
        adverts = [...adverts, advert]
      }});
    if(this.state.filters === 'priceDescending') {
      await adverts.sort((a, b) => (b.amount_payment - a.amount_payment))
    } else if (this.state.filters === "priceAscending") {
      await adverts.sort((a, b) => (a.amount_payment - b.amount_payment))
    } else if (this.state.filters ===  "fromOldToNew") {
      await adverts.sort((a, b) => (a.id - b.id))
    } else if (this.state.filters ===  "fromNewToOld") {
      await adverts.sort((a, b) => (b.id - a.id))
    }
    const slice = await adverts && adverts.slice(this.state.offset, this.state.offset + this.state.perPage);
    let postDataList = await slice && slice.map(advert => {
      return (
        <NavLink key={advert && advert.id} className="link_about_advert_list" tag={RouterNavLink} to={`/about_advert/${advert.id}`} >
          <div className="inline_card">
            <img
              className={advert.images ? "img_logo " : "img_inline "}
              src={advert.images ? `${config.apiURL}/uploads/adverts/${advert && advert.images.name}` :  Logo}
              alt={advert.images ?  advert && advert.images.name  :  Logo}/>
            <div className="card_inline_text">
              <p className={this.props.loading ? "text_color" : "heading_card"}>{this.props.loading ? "Заголовок" : advert && advert.title}</p>
              <p className={this.props.loading ? "text_color" : null}>{this.props.loading ? "Цена" : advert.username && advert.username.substr(0, 3) === "996" && advert.username.substr(1, 4) ===  "996"  ?
                advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} тенге`
                : advert.amount_payment === 1 || advert.amount_payment === 111 ? "Договорная" : `${advert && advert.amount_payment} сом`}</p>
              <p className={this.props.loading ? "text_color" : "title_card_inline"}>{this.props.loading ? "Описание" : advert && advert.text}</p>
            </div>
          </div>
        </NavLink>
      );
    });
    this.setState({
      pageCount: Math.ceil( adverts && adverts.length / this.state.perPage),
      postDataList
    })
  };

  handlePageClick = (e) => {
    window.scroll(0,100);
    const selectedPage = e.selected;
    localStorage.setItem('selectedPage', selectedPage );
    const offset = selectedPage * this.state.perPage;
    this.setState({
      currentPage: selectedPage,
      offset: offset
    }, () => {
      this.state.view === 'module' ? this.fetchSearchPlaces() : this.fetchSearchPlacesList();
    });
  };

  handleChange = (view) => {
    let viewBlock = null;
    view === null ? viewBlock = this.state.view : viewBlock = view;
    this.setState({view: viewBlock}, () => {
      this.state.view === 'module' ? this.fetchSearchPlaces() : this.fetchSearchPlacesList();
    })
  };

  handleRange = value => {
    this.setState({
      value: value
    });
    this.state.view === 'module' ? this.fetchSearchPlaces() : this.fetchSearchPlacesList();
  };

  handleChangeFilters = (event) => {
    this.setState({filters: event.target.value}, () => this.state.view === 'module' ? this.fetchSearchPlaces() : this.fetchSearchPlacesList());
  };
  render() {
    return (
      <Container>
        {this.props.search && this.props.search.length === 0 ?
          <div className="no-found">
            <h3 className="heading heading_center">Ничего не найдено</h3>
            <img src={noFound} alt="не найдено"/>
          </div> : <div className="heading_title">
            <h3 className="heading">Найденые объявления</h3>
            <FormControl className='filter_menu'>
              <InputLabel id="filter">Фильтры</InputLabel>
              <Select
                labelId="filter"
                id="filter"
                value={this.state.filters}
                onChange={this.handleChangeFilters}
              >
                <MenuItem value="priceDescending">По цене (по убыванию)</MenuItem>
                <MenuItem value="priceAscending">По цене (по возрастанию)</MenuItem>
                <MenuItem value="fromOldToNew">От старого до нового</MenuItem>
                <MenuItem value="fromNewToOld">От нового до старого</MenuItem>
              </Select>
            </FormControl>
            {/*<AccordionComponent heading="Ценовой диапазон" componentChildren={<SliderComponent value={this.state.value} handleRange={this.handleRange} value1={this.state.value[0]} value2={this.state.value[1]}/>}/>*/}
            <ToggleButtons view={this.state.view} handleChanges={this.handleChange}/>
          </div>}
        <div className="cards">
          {this.state.view === "module" ? this.state.postData : this.state.postDataList}
        </div>
        {this.state.postData ? <ReactPaginate
          initialPage={0}
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={"..."}
          breakClassName="break-me"
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={3}
          onPageChange={this.handlePageClick}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName={"active_pagination"} /> : null}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search.search,
  loading: state.search.loading,
});


export default connect(mapStateToProps, null)(SearchContainer);
