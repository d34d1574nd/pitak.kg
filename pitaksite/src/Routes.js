import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import MainPage from "./containers/MainPage/MainPage";
import ModalWindow from "./components/modalWindow/modalWindow";
import AboutOneAdvert from "./containers/AboutOneAdvert/AboutOneAdvert";
import AdminPage from "./containers/AdminPage/AdminPage";
import AddAutoAndAdvertForm from "./containers/AddAutoAndAdvertForm/AddAutoAndAdvertForm";
import SearchContainer from "./containers/SearchContainer/SearchContainer";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
);

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/search" exact component={SearchContainer}/>
            <Route path="/register" exact component={ModalWindow}/>
            <Route path="/login" exact component={ModalWindow}/>
            <Route path="/about_advert/:advert" exact component={AboutOneAdvert}/>
            <ProtectedRoute isAllowed={user} path="/add_advert"
                            component={AddAutoAndAdvertForm}/>
            <ProtectedRoute isAllowed={user} path="/edit_advert"
                          component={AddAutoAndAdvertForm}/>
            <ProtectedRoute isAllowed={user && user.role.name === 'ROLE_ADMIN'} path="/admin"
                            component={AdminPage}/>
        </Switch>
    );
};

export default Routes;
