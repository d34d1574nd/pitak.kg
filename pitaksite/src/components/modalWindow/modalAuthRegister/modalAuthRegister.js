import React, {Fragment} from 'react';
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import FormAuth from "./formAuth/formAuth";
import FormRegister from "./formRegister/formRegister";

import "./modalAuthRegister.css";

const ModalAuthRegister = ({loginFunc, registerFunc, windowModal}) => {
  return (
    <Fragment>
      <div className="navigation_modal">
        <NavLink tag={RouterNavLink} to="/login" onClick={loginFunc} className={`${windowModal ? "login" : "register"} links`}>
          Войти
        </NavLink>
        <NavLink tag={RouterNavLink} to="/register" onClick={registerFunc} className={`${windowModal ? "register" : "login"} links`}>
          Регистрация
        </NavLink>
      </div>
      <div className="body_modal">
        {windowModal ? <FormAuth/> : <FormRegister/>}
      </div>
    </Fragment>
  );
};

export default ModalAuthRegister;
