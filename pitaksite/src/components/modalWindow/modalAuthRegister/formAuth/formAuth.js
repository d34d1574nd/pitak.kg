import React, {Component} from 'react';
import {connect} from "react-redux";
import {Input, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import InputMask from "react-input-mask";

import IconUser from "../../../../assets/userIcon.svg";

import "./formAuth.css";
import Button from "@material-ui/core/Button";
import {loginUser} from "../../../../store/action/userActions/userActions";
import {fetchCountries} from "../../../../store/action/dictionaryActions/dictionaryActions";
import Recaptcha from "react-recaptcha";
import {NotificationManager} from "react-notifications";

class FormAuth extends Component {
  state = {
    username: '',
    password: '',
    hiddenPassword: true,
    "countryModel.id": "",
    verifyUser: false
  };

  componentDidMount() {
    this.props.fetchCountries();
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    // if (this.state.verifyUser === false) {
    //   NotificationManager.error('Неверная капча');
    // } else {
      this.props.loginUser({...this.state});
    // }

  };

  toggleShowPassword = () => {
    this.setState({hiddenPassword: !this.state.hiddenPassword})
  };

  callback = () => {
    return;
    // console.log('Done!!!');
  };

  verifyCallback = (response) => {
    if (response) {
      this.setState({
        verifyUser: true
      })
    }
  };

  render() {
    return (
      <form className="form_auth" onSubmit={this.submitFormHandler}>
        <p><img src={IconUser} alt="icon user"/></p>
        <div className="custom_select custom_input-1 ">
          <select className="selects"
                  name="countryModel.id" value={this.state["countryModel.id"]}
                  onChange={this.inputChangeHandler}
          >
            <option value="">Выберите страну</option>
            {this.props.countries && this.props.countries.map(country => (
              <option key={country.id} value={country.id}>{country.name}</option>
            ))}
          </select>
          <div className="icon-arrow"/>
        </div>
        {this.state['countryModel.id'] ? Number(this.state['countryModel.id']) === 2 ?
          <div className="custom_input custom_input-2">
            <Input
              type="tel" className="inputs input-2"
              placeholder="Номер телефона" name="username"
              value={this.state.username}
              mask="\9\96999999999"
              maskChar="_"
              tag={InputMask}
              onChange={this.inputChangeHandler}
            />
          </div> :
          <div className="custom_input custom_input-2">
            <Input
              type="tel" className="inputs input-2"
              placeholder="Номер телефона" name="username"
              value={this.state.username}
              mask="79999999999"
              maskChar="_"
              tag={InputMask}
              onChange={this.inputChangeHandler}
            />
          </div> : null}
        <div className="custom_input custom_input-5">
          <input
            type={this.state.hiddenPassword ? "password" : "text"}
            className="inputs input-5"
            placeholder="Пароль" name="password"
            onChange={this.inputChangeHandler}
            value={this.state.password}
          />
          <Button color="primary" className="buttonPassword" onClick={this.toggleShowPassword}>
            {this.state.hiddenPassword === false ? <i className="far fa-eye"/> : <i className="far fa-eye-slash"/>}
          </Button>
        </div>
        <div className="captcha">
          <Recaptcha
            sitekey="6LcOXxAaAAAAAKEUGdW0IxVbJejkwh9XOvg96uYW"
            hl="ru" render="explicit" type='image'
            verifyCallback={this.verifyCallback}
            onloadCallback={this.callback}
          />
        </div>
        <div className="buttons">
          <button type="submit">Войти</button>
          {/*<NavLink className="forgot_pass" tag={RouterNavLink} to="/">Забыли пароль?</NavLink>*/}
        </div>
      </form>
    );
  }
}
const mapStateToProps = state => ({
  countries: state.dictionary.countries,
});

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(loginUser(userData)),
  fetchCountries: () => dispatch(fetchCountries()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormAuth);
