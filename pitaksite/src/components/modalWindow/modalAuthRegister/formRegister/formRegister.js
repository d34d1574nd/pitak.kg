import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import InputMask from "react-input-mask";
import {FormText, Input,} from "reactstrap";
import Button from "@material-ui/core/Button";
import zxcvbn from 'zxcvbn';
import Recaptcha from 'react-recaptcha'


import {registerUser} from "../../../../store/action/userActions/userActions";
import {
  fetchCities,
  fetchCountries
} from "../../../../store/action/dictionaryActions/dictionaryActions";

import IconUser from "../../../../assets/addUserPhoto.svg";
import SyncIcon from '@material-ui/icons/Sync';

import "./formRegister.css";
import {NotificationManager} from "react-notifications";


class FormRegister extends Component {
  state = {
    username: '',
    userType: 1,
    password: '',
    name: "",
    passwordSecond: "",
    "countryModel.id": "",
    hiddenPassword: true,
    testedResult: "",
    profilePhoto: null,
    imgSrc: "",
    modal: false,
    verifyUser: false,
  };

  componentDidMount() {
    this.props.fetchCountries();
  }

  inputChangeHandler = event => {
    const testedResult = zxcvbn(this.state.password);
    this.setState({
      [event.target.name]: event.target.value,
      testedResult: testedResult.score
    })
  };

  fileChangeHandler = event => {
    const file = event.target.files[0];
    const fileValidation = /(.*?)\.(jpg|bmp|jpeg|png|img)$/;
    if (event.target.value.match(fileValidation)) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function (e) {
        this.setState({
          imgSrc: [reader.result]
        })
      }.bind(this);
      this.setState({
        [event.target.name]: event.target.files[0]
      })
    } else {
      NotificationManager.error('Не поддерживаемый формат файла')
    }

  };

 toggleShowPassword = () => {
    this.setState({hiddenPassword: !this.state.hiddenPassword})
  };

  ChangePass = () => {
    this.setState({
      show: true,
    })
  };

  ClosePassCorrect = () => {
    this.setState({
      show: false
    })
  };

  PasswordCorrect = result => {
    switch (result) {
      case 0:
        return 'Очень легкий';
      case 1:
        return 'Легкий';
      case 2:
        return 'Средний';
      case 3:
        return 'Хороший';
      case 4:
        return 'Сильный';
      default:
        return 'Легкий';
    }
  };

  submitFormHandler = event => {
    event.preventDefault();
    const regularExpression = /(?=.*[a-z])(?=.*[0-9])/;
    const passCorrect = regularExpression.test(this.state.password);
    if (passCorrect === false) {
      NotificationManager.error('Пароль должен содержать прописные буквы и цифры!');
    } else if (passCorrect === true && this.state.password.length < 6) {
      NotificationManager.error('Пароль должен быть от 6 символов!');
    } else if (passCorrect === true && this.state.password !== this.state.passwordSecond) {
      NotificationManager.error('Пароли не верны !');
    }
    // else if (this.state.verifyUser === false) {
    //   NotificationManager.error('Неверная капча');
    // }
    else {
      const formData = new FormData();

      Object.keys(this.state).forEach(key => {
        formData.append(key, this.state[key]);
      });
      this.props.registerUser(formData)
    }
  };

  checkModalAuto = () => {
    this.setState({
      modal: true
    })
  };

  callback = () => {
    return;
  };

  verifyCallback = (response) => {
    if (response) {
      this.setState({
        verifyUser: true
      })
    }
  };


  render() {
    return (
      <form className="form_register" onSubmit={this.submitFormHandler}>
        <div className="input_image">
          <input id="file-input" type="file" name="profilePhoto" onChange={this.fileChangeHandler}/>
            <label htmlFor="file-input">
              {this.state.profilePhoto !== null ?
                <div className="image">
                  <img className="user_image" src={this.state.imgSrc[0]} alt="icon user"/>
                  <div className="plus"><SyncIcon/></div>
                </div> :
              <img src={IconUser} alt="user"/>
              }
            </label>
        </div>
        <FormText color="muted" className="form_text">
          Загрузите фотографию (Не обязательно)
          <p> Поддерживаются файлы формата: .png, .img, .jpeg, .bmp, .jpg</p>
        </FormText>
        <div className="custom_select custom_input-1 ">
          <select className="selects"
                  name="countryModel.id" value={this.state["countryModel.id"]}
                  onChange={this.inputChangeHandler} onClick={() => this.props.fetchCities(this.state['countryModel.id'])}
          >
            <option value="">Выберите страну *</option>
            {this.props.countries && this.props.countries.map(country => (
              <option key={country.id} value={country.id}>{country.name}</option>
            ))}
          </select>
          <div className="icon-arrow"/>
        </div>
        {this.state['countryModel.id'] ?
            <Fragment>
              {Number(this.state['countryModel.id']) === 2 ?
                <div className="custom_input custom_input-2">
                  <Input
                    type="tel" className="inputs input-2"
                    placeholder="Номер телефона  *" name="username"
                    value={this.state.username}
                    mask="\9\96999999999"
                    maskChar="_"
                    tag={InputMask}
                    onChange={this.inputChangeHandler}
                  />
                </div> :
                <div className="custom_input custom_input-2">
                  <Input
                    type="tel" className="inputs input-2"
                    placeholder="Номер телефона  *" name="username"
                    value={this.state.username}
                    mask="79999999999"
                    maskChar="_"
                    tag={InputMask}
                    onChange={this.inputChangeHandler}
                  />
                </div>}
            </Fragment>
          : null}

        <div className="custom_input custom_input-3">
          <input type="text" onChange={this.inputChangeHandler} value={this.state.name} name="name" className="inputs input-5" placeholder="Введите имя *"/>
        </div>

        {/*<div className="custom_select custom_select-4 ">*/}
        {/*  <select className="selects" name="userType" onChange={this.inputChangeHandler} value={this.state.userType}>*/}
        {/*    <option value="">Тип пользователя</option>*/}
        {/*    <option value="2">Клиент</option>*/}
        {/*    <option value="1">Водитель</option>*/}
        {/*  </select>*/}
        {/*  <div className="icon-arrow"/>*/}
        {/*</div>*/}


        <div className="custom_input custom_input-5">
          <input
            type={this.state.hiddenPassword ? "password" : "text"}
            className="inputs input-5"
            placeholder="Введите пароль *" name="password"
            onChange={this.inputChangeHandler}
            onClick={this.ChangePass}
            value={this.state.password}
          />
          <Button color="primary" className="buttonPassword" onClick={this.toggleShowPassword}>
            {this.state.hiddenPassword === false ? <i className="far fa-eye"/> : <i className="far fa-eye-slash"/>}
          </Button>
        </div>
        {this.state.show === true ?
          <Fragment>
            <div className="password-strength-meter">
            <progress
              className={`password-strength-meter-progress strength-${this.PasswordCorrect(this.state.testedResult)}`}
              value={this.state.testedResult}
              max="6"
            />
            </div>
          <label className="password-strength-meter-label">
            {this.state.password && (
                <>
                  <strong>Сложность пароля : </strong> {this.PasswordCorrect(this.state.testedResult)}
                </>
              )}
          </label>
          </Fragment>
          : null}
        <div className="custom_input custom_input-5">
          <Input
            className="inputs input-5" name="passwordSecond"
            type={this.state.hiddenPassword ? "password" : "text"}
            onClick={this.ClosePassCorrect}
            value={this.state.passwordSecond}
            onChange={this.inputChangeHandler}
            placeholder="Введите пароль еще раз *"
          />
        </div>
        <div className="captcha">
          <Recaptcha
            sitekey="6LcOXxAaAAAAAKEUGdW0IxVbJejkwh9XOvg96uYW"
            hl="ru" render="explicit" type='image'
            verifyCallback={this.verifyCallback}
            onloadCallback={this.callback}
          />
        </div>
        <div className="buttons">
          <button type="submit">Зарегистрироваться</button>
        </div>
      </form>
    );
  }
}
const mapStateToProps = state => ({
  error: state.user.registerError,
  countries: state.dictionary.countries,
  cities: state.dictionary.cities,
});


const mapDispatchToProps = dispatch => ({
  registerUser: (userData) => dispatch(registerUser(userData)),
  fetchCountries: () => dispatch(fetchCountries()),
  fetchCities: (parentId) => dispatch(fetchCities(parentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormRegister);
