import React, {Component, Fragment} from 'react';

import Backdrop from "../UI/Backdrop/Backdrop";
import ModalAuthRegister from "./modalAuthRegister/modalAuthRegister";
import MainPage from "../../containers/MainPage/MainPage";

import Cross from "../../assets/cross.svg";

import "./modalWindows.css";
import "./modalMedia.css";


class ModalWindow extends Component {
  state = {
    modal: true,
    windowModal: true
  };

  handleClose = () => {
    this.setState({modal: false});
    this.props.history.push('/')
  };

  loginFunc = () => {
    this.setState({
      windowModal: true
    });
  };

  registerFunc = () => {
    this.setState({
      windowModal: false
    })
  };

  render() {
    return (
      <Fragment>
        <MainPage/>
        <Backdrop show={this.state.modal} onClick={this.handleClose}/>
        <div className={this.state.modal ? `${this.state.windowModal === false ? "modal_false" : null} modal_window` : "modal_false"}>
          <img src={Cross} alt="cross exit" onClick={this.handleClose} className="cross"/>
          <ModalAuthRegister loginFunc={this.loginFunc} registerFunc={this.registerFunc} windowModal={this.state.windowModal}/>
        </div>
      </Fragment>
    );
  }
}

export default ModalWindow;
