import React from 'react';

import "./imageSlider.css";
import ImageGallery from "react-image-gallery";
import Spinner from "../UI/Spinner/Spinner";
import config from "../../config";

const ImageSlider = ({loading, image}) => {
  if (loading) {
    return <Spinner/>
  } else {
    let images = [];
    for(let i = 0; i < image.length; i++) {
      images = [...images, {
        original: `${config.apiURL}/uploads/adverts/${image[i].name}`,
        thumbnail: `${config.apiURL}/uploads/adverts/${image[i].name}`
      }]
    }
    return (
      <div className="slider">
        <ImageGallery items={images} showPlayButton={false} autoPlay={true} slideDuration={1000}/>
      </div>
    );
  }
};

export default ImageSlider;
