import React from 'react';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";

import "./cardComponent.css";
// import Avatar from "@material-ui/core/Avatar";

const CardComponent = ({loading, key, heading, image, price, title, name}) => {
  return (
    <Card className="card_auto" key={key}>
      <CardActionArea>
        <CardMedia
          className={loading ? "img_div" : image ===  "/static/media/logo.9a04cfe4.svg" ? "img img_pos" : "img"}
          key={key}
          image={image}
          title={title}
        />
        <CardContent>
          <p className={loading ? "text_color" : "heading_card"}>{loading ? "Заголовок" : heading}</p>
          <p className={loading ? "text_color" : null}>{loading ? "Цена" : price}</p>
          <p className={loading ? "text_color" : "title_card"}>{loading ? "Описание" : title}</p>
          {/*<div className="user">*/}
          {/*  <Avatar>{name}</Avatar>*/}
          {/*  <span>2 часа назад</span>*/}
          {/*</div>*/}
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default CardComponent;
