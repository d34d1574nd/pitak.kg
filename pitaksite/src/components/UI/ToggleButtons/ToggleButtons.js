import React, {useState} from 'react';
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ViewListIcon from "@material-ui/icons/ViewList";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
const ToggleButtons = ({handleChanges}) => {

  let [view, setView] = useState('module');

  let handleChange = (event, nextView) => {
    setView(nextView);
    handleChanges(nextView);
  };

  return (
    <ToggleButtonGroup className="button_toggle" orientation="horizontal" value={view} exclusive onChange={handleChange}>
      <ToggleButton value="module" aria-label="module">
        <ViewModuleIcon />
      </ToggleButton>
      <ToggleButton value="list" aria-label="list" >
        <ViewListIcon />
      </ToggleButton>
    </ToggleButtonGroup>
  );
};

export default ToggleButtons;
