import React from 'react';
import PropTypes from 'prop-types';
import {Input} from "reactstrap";

const FormElement = ({propertyName, onClick, className, ...props}) => {
  return (
        <Input
          name={propertyName} id={propertyName} className={className}
          onClick={onClick} placeholder={propertyName}
          {...props}
        />
  );
};

FormElement.propTypes = {
  propertyName: PropTypes.string.isRequired
};

export default FormElement;
