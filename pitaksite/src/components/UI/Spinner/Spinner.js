import React from 'react';

import './Spinner.css';
import Backdrop from "../Backdrop/Backdrop";

const Spinner = () => {
  return (
    <div className='Spinner'>
      <Backdrop show={true}/>
      <div className="lds-facebook">
        <div/>
        <div/>
        <div/>
      </div>
    </div>
  );
};

export default Spinner;
