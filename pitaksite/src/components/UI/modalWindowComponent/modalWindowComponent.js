import React, {Fragment} from 'react';

import Backdrop from "../Backdrop/Backdrop";

import Cross from "../../../assets/cross.svg";

import "./modalWindowComponent.css";
import "./modalMedia.css";
import Button from "@material-ui/core/Button";

const ModalWindowComponent = ({role, modal, windowModal, handleClose, deleteAdvert, additionalDescription, inputChangeHandler, reportTypeId, changeCheckbox, submit}) => {
  return (
      <Fragment>
        <Backdrop show={modal} onClick={handleClose}/>
        <div className={modal ? `${windowModal === false ? "modal_false" : null} modal_window` : "modal_false"}>
          <img src={Cross} alt="cross exit" onClick={handleClose} className="cross"/>
          {role && role === "ROLE_ADMIN" ?
            <Fragment>
              <h5>Уверены что хотите удалить это обьявление?</h5>
              <div className="delete_modal">
                <Button color="secondary" onClick={deleteAdvert}>Да</Button>
                <Button  color="primary" onClick={handleClose}>Нет</Button>
              </div>
            </Fragment>
          :
          <Fragment>
              <h5>Выберите причину <span>*</span></h5>
              <form className="modal_body" onSubmit={submit}>
                <p className="check_form">
                  <input type="radio" id="reportTypeId1" name="reportTypeId" onChange={changeCheckbox} value="1"/>
                  <label htmlFor="reportTypeId1">Запрещенный товар/услуга</label>
                </p>
                <p className="check_form">
                  <input type="radio" id="reportTypeId2" name="reportTypeId" onChange={changeCheckbox} value="2"/>
                  <label htmlFor="reportTypeId2">Дубликат</label>
                </p>
                <p className="check_form">
                  <input type="radio" id="reportTypeId3" name="reportTypeId" onChange={changeCheckbox} value="3"/>
                  <label htmlFor="reportTypeId3">Неверна категория</label>
                </p>
                <p className="check_form">
                  <input type="radio" id="reportTypeId4" name="reportTypeId" onChange={changeCheckbox} value="4"/>
                  <label htmlFor="reportTypeId4">Мошенничество</label>
                </p>
                <p className="check_form">
                  <input type="radio" id="reportTypeId5" name="reportTypeId" onChange={changeCheckbox} value="5"/>
                  <label htmlFor="reportTypeId5">Другая причина</label>
                </p>
                <h5>Опишите вашу жалобу <span>*</span></h5>
                <textarea className="complaint" minLength="20" required onChange={inputChangeHandler} value={additionalDescription} name="additionalDescription" cols="25" rows="5" placeholder="Минимум 20 символов"/>
                <div className="buttons">
                  <Button type="submit" variant="contained" color="primary">Отправить</Button>
                </div>
              </form>
            </Fragment>
          }
          </div>
      </Fragment>
  );
};

export default ModalWindowComponent;

