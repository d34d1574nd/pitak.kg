import React from 'react';
import Container from "@material-ui/core/Container";
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import Logo from "../../assets/Logo_footer.svg";
import AppStore from "../../assets/appStore.png";
import GoogleStore from "../../assets/googleStore.png";

import "./Footer.css";
import "./FooterMedia.css";

const Footer = () => {
  return (
    <Container>
      <footer>
        <div className="footer">
          <div className="block">
            <NavLink tag={RouterNavLink} to="/">
              <img src={Logo} alt="Pitak.kg"/>
            </NavLink>
            <p>© 2020 Все права защищены</p>
          </div>
          <div className="store">
            <a className="nav-link" href="https://apps.apple.com/kh/app/pitak/id1517677384">
              <img src={AppStore} alt="app store pitak" className="img_store"/>
            </a>
            <a className="nav-link" href="https://play.google.com/store/apps/details?id=com.nextinnovation.pitak">
              <img src={GoogleStore} alt="google store pitak" className="img_store"/>
            </a>
          </div>
        </div>
      </footer>
    </Container>
  );
};

export default Footer;
