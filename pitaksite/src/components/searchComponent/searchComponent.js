import React, {Component} from 'react';
import {connect} from "react-redux";
import Container from "@material-ui/core/Container";
import { withRouter } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';

import {
  fetchCarType,
  fetchCities,
  fetchCountries
} from "../../store/action/dictionaryActions/dictionaryActions";

import "./searchComponent.css";
import "./searchMedia.css";
import {fetchSearch} from "../../store/action/searchAction/searchAction";
import {fetchAdverts} from "../../store/action/advertsActions/advertsActions";

class SearchComponent extends Component {

  state = {
    country: "",
    text: "",
    car_type_id: "",
    city: ""
  };

  componentDidMount() {
    this.props.fetchCountries();
    this.props.fetchCarType();
    this.props.fetchAdverts();
    this.props.fetchCities(2);
  }

  submitFormHandler = async () => {
    let search = [];
    let searchAllParams = [];
    if(this.state.car_type_id !== "") {
      for(let i = 0; i < this.props.adverts.length; i++) {
        if (this.state.car_type_id === this.props.adverts[i].car.car_type.id) {
          search = [...search, this.props.adverts[i]];
        }
      }
      this.props.fetchSearch(search);
      this.props.history.push('/search');
    }
    if(this.state.city !== '') {
      for(let i = 0; i < search.length; i++) {
        if (this.state.city === search[i].city_id) {
          searchAllParams = [...searchAllParams, search[i]];
        }
      }
      this.props.fetchSearch(searchAllParams);
      this.props.history.push('/search');
    }
    if(this.state.car_type_id === '' && this.state.city === '') {
      this.props.fetchSearch(this.props.adverts);
      this.props.history.push('/search');
    }
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };



  render() {
    let type = this.props.carType && this.props.carType.sort((a, b) => {
      return b.id - a.id
    });
    let typesCar = type.slice(1, 21);
    let cities = this.props.cities && this.props.cities.sort((a, b) => {
      return b.id - a.id
    });
    let allCities = [...cities.slice(0, 69), ...cities.slice(71, 76)];
    allCities.reverse();
    return (
      <div className="search">
        <Container>
          <div className="search_form">
            <form className="form_search">
              <select className="registerAuto input_form input_type_media" name="car_type_id" onChange={this.inputChangeHandler} onTouchEnd={this.submitFormHandler} onClick={this.submitFormHandler} value={this.state.car_type_id}>
                <option value="" onTouchEnd={this.submitFormHandler}>Тип транспорта</option>
                {this.props.carType && typesCar.map(type => (
                  <option key={type.id} value={type.id} onTouchEnd={this.submitFormHandler}>{type.name}</option>
                ))}
              </select>
              {/*<select name="countryModel.id" value={this.state["countryModel.id"]} className="country_input input_form"*/}
              {/*        onChange={this.inputChangeHandler} onClick={() => this.props.fetchCities(this.state['countryModel.id'])}*/}
              {/*  >*/}
              {/*  <option value="">Cтрана</option>*/}
              {/*  {this.props.countries && this.props.countries.map(country => (*/}
              {/*    <option key={country.id} value={country.id}>{country.name}</option>*/}
              {/*  ))}*/}
              {/*</select>*/}
              {this.state.car_type_id ? <select name="city" className="registerAuto input_form" onChange={this.inputChangeHandler}  onTouchEnd={this.submitFormHandler} onClick={this.submitFormHandler}
                        value={this.state.cityId}
              >
                <option value="" onTouchEnd={this.submitFormHandler}>Все города</option>
                {this.props.cities && allCities.map(city => (
                  <option onTouchEnd={this.submitFormHandler} key={city.id} value={city.id}>{city.name}</option>
                ))}
              </select>
              : null}
              {/*<button type="submit" className="button_search_mini"><SearchIcon/></button>*/}
              {/*<button type="submit" className="button_search">Найти</button>*/}
            </form>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  adverts: state.adverts.adverts,
  countries: state.dictionary.countries,
  cities: state.dictionary.cities,
  carType: state.dictionary.carType,
});


const mapDispatchToProps = dispatch => ({
  fetchCountries: () => dispatch(fetchCountries()),
  fetchCities: (parentId) => dispatch(fetchCities(parentId)),
  fetchCarType: () => dispatch(fetchCarType()),
  fetchSearch: (search) => dispatch(fetchSearch(search)),
  fetchAdverts: () => dispatch(fetchAdverts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchComponent));
