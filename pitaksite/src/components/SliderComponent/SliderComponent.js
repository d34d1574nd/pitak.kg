import React from 'react';
import {Range} from "rc-slider";

import 'rc-slider/assets/index.css';
import "./SliderComponent.css";

const SliderComponent = ({value1, value2, value, handleRange}) => {
  return (
    <div className="range">
      <Range min={200} max={3000} pushable={true} value={value} defaulValue={value} onChange={handleRange} step={50}/>
      <div className="numbers">
        <span>{value1}</span>
        <span>{value2}</span>
      </div>
    </div>
  );
};

export default SliderComponent;
