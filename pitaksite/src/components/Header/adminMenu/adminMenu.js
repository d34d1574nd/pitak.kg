import React from 'react';
import Avatar from "@material-ui/core/Avatar";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";

import "./adminMenu.css";
import "./adminMedia.css";

const AdminMenu = ({user, logout}) => {
  return (
    <div className="admin_menu">
      <NavLink tag={RouterNavLink} className="user_menu-links link_admin" to="/admin"><Avatar>{user.name[0]}</Avatar> <span className="user_link">{user.name} </span></NavLink>
      <NavLink tag={RouterNavLink} onClick={logout} className="user_menu-links link_logout" to="/">
        <span>Выйти</span>
      </NavLink>
    </div>
    )
  };

export default AdminMenu;
