import React from 'react';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Avatar from "@material-ui/core/Avatar";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";

import "./userMenu.css";

const UserMenu = ({user, logout}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button aria-controls="simple-menu" className={`button_user ${Boolean(anchorEl) ? `active_button` : null}`} aria-haspopup="true" onClick={handleClick}>
        <Avatar>{user.name[0]}</Avatar> <span className="user_link">{user.name} <i className="fas fa-sort-down"/></span>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Мои заказы</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Мои машины</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Мои объявления</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Метод оплаты</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Промокоды</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Настройки</span>
          </NavLink>
        </MenuItem>
        <MenuItem onClick={logout}>
          <NavLink tag={RouterNavLink} className="user_menu-links" to="/">
            <span>Выйти</span>
          </NavLink>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default UserMenu;
