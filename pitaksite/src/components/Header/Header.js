import React from 'react';
import Container from "@material-ui/core/Container";
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import UserMenu from "./userMenu/userMenu";

import Logo from "../../assets/logo.svg";
import AddIcon from '@material-ui/icons/Add';

import "./Header.css";
import "./HeaderMedia.css";
import AdminMenu from "./adminMenu/adminMenu";

const Header = ({user, logout}) => {
  return (
    <header>
      <Container>
          <div className="menu">
            <div className="menu_logo">
              <NavLink tag={RouterNavLink} to="/" className="logo">
                <img src={Logo} alt="Pitak.kg"/>
              </NavLink>
              <NavLink tag={RouterNavLink} to="/add_advert" className="add_advert">
                <AddIcon/> <span>Подать объявление</span>
              </NavLink>
            </div>
            {
              user ?
                user.role.name === "ROLE_ADMIN" ? <AdminMenu user={user} logout={logout}/> : <UserMenu user={user} logout={logout}/>
                :
              <NavLink className="register_link" tag={RouterNavLink} to="/register">
              Вход и регистрация
            </NavLink>
            }
          </div>
      </Container>
    </header>
  );
};

export default Header;
