import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import { CookiesProvider } from 'react-cookie';
import store, {history} from "./store/configureStore";
import App from './App';

import './index.css';
import 'react-notifications/lib/notifications.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-tabs/style/react-tabs.css";


const app = (
  <CookiesProvider>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App/>
      </ConnectedRouter>
    </Provider>
  </CookiesProvider>
);

ReactDOM.render(app, document.getElementById('root'));

