const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPathPlaceAdvert: path.join(rootPath, 'public/uploads/adverts'),
    uploadPathPlaceCar: path.join(rootPath, 'public/uploads/car'),
};
