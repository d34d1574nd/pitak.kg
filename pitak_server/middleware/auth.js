const Pool = require('pg').Pool;

const pool = new Pool({
  user: 'postgres',
  host: '176.123.246.251',
  database: 'db_transportation',
  password: 'Jankam31',
  port: 5432,
});

const auth = async (req, res, next) => {
    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'Token not provided'})
    }
  await pool.query('SELECT * FROM public.users WHERE token = $1',
    [token], async (error, results) => {
      if (error) {
        throw error;
      }
      pool.query(`SELECT * FROM user_roles WHERE user_id = ${results.rows[0].id}`, (error, result) => {
        pool.query(`SELECT * FROM roles WHERE id = ${result.rows[0].role_id}`, (err, res) => {
          const username = !results.rows[0].username;
          if (username) {
            return res.status(401).send({error: 'Token incorrect'});
          }
          req.user = {...results.rows[0], role: res.rows[0]};
          next()
        })
      });
    })
};

module.exports = auth;
