"use strict";

const express = require('express');
const cors = require('cors');

const user = require('./app/users');
const dictionaries = require('./app/dictionaries');
const advert = require('./app/advert');
const admin = require('./app/admin');
const add = require('./app/add');
const report = require('./app/report');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use('/users', user);
app.use('/dictionaries', dictionaries);
app.use('/advert', advert);
app.use('/admin', admin);
app.use('/add', add);
app.use('/report', report);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});

require("greenlock-express")
  .init({
      packageRoot: __dirname,
      configDir: "./greenlock.d",

      // contact for security and critical bug notices
      maintainerEmail: "d34d1574nd@mail.ru",

      // whether or not to run at cloudscale
      cluster: false
  })
  // Serves on 80 and 443
  // Get's SSL certificates magically!
  .serve(app);

