const Pool = require('pg').Pool;
const {nanoid} = require('nanoid');
const bcrypt = require('bcrypt');
const express = require('express');
const multer = require('multer');
const path = require('path');

const auth = require('../middleware/auth');
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathPlaceUser);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const SALT_WORK_FACTOR = 10;

const router = express.Router();

const pool = new Pool({
    user: 'postgres',
    host: '176.123.246.251',
    database: 'db_transportation',
    password: 'Jankam31',
    port: 5432,
});

router.post('/', [upload.single('image')], async (request, response) => {
  const { username, password, userType, name } = request.body;
  let image = null;
  if (request.file) {
    image = request.file.filename;
  }
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(password, salt);
  this.password = hash;

  pool.query('SELECT id, username, role_id FROM public.users LEFT JOIN user_roles ON users.id = user_roles.user_id WHERE username = $1',
    [username], async (error, results) => {
      if (error) {
        throw error;
      }
      let user ={};
      let role ={};
      if (results.rows && results.rows[0] && results.rows[0].username === username ){
        return response.status(200).json({message: 'Такой пользователь уже существует'});
      } else {
        await pool.query('SELECT * FROM public.users', (err, result) => {
          let number_id = result.rows.length + 4000 + results.rows.length;
          pool.query('INSERT INTO public.users (id, username, name, password, token, profile_photo) VALUES ($1, $2, $3, $4, $5, $6) RETURNING * ',
            [number_id++, username, name, this.password, "Bearer " + nanoid(40), image], (error, result) => {
              if (error) {
                throw error;
              }
              pool.query("SELECT * FROM public.roles WHERE id = $1", [userType], (err, results) => {
                role = {id: results.rows[0].id, name: results.rows[0].name};
                pool.query('INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2) RETURNING *',
                  [result.rows[0].id, results.rows[0].id], async (err, res) => {
                    user = {...result.rows[0], role};
                    return await response.status(201).json(user);
                  })
              });
            })
        });
      }
    })
});


router.post('/sessions', async (request, response) => {
  const { username, password } = request.body;

  await pool.query('SELECT * FROM public.users WHERE username = $1',
    [username], async (error, results) => {
      if (error) {
        throw error;
      }
      let users = [];
      for (let i = 0; i < results.rows.length; i++) {
        await bcrypt.compare(password, results.rows[i].password, async function(err, res) {
          if (res) {
            await pool.query(`SELECT * FROM user_roles WHERE user_id=${results.rows[i].id}`, async (error, result) => {
              await pool.query(`SELECT * FROM roles WHERE id=${result.rows[0].role_id}`, async (err, res) => {
                users = {...results.rows[i], role: {id: res.rows[0].id, name: res.rows[0].name}};
                return await response.status(201).json(users);
              })
            });
          }
        });
      }
    });
});

router.delete('/sessions', [auth], async (request,response) => {
  const token = request.get('Authorization');

  await pool.query('SELECT username, token FROM public.users WHERE token = $1',
    [token], (error, results) => {
      if (error) {
        throw error;
      }
      pool.query(
        'UPDATE public.users SET token = $1 WHERE username = $2 RETURNING username, token',
        ["Bearer " + nanoid(40), results.rows[0].username],
        (error, results) => {
          if (error) {
            throw error
          }
          response.status(201).send({message: 'Logged out'});
        }
      );
    });
});



module.exports = router;
