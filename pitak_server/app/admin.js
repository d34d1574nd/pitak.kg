const Pool = require('pg').Pool;
const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const pool = new Pool({
  user: 'postgres',
  host: '176.123.246.251',
  database: 'db_transportation',
  password: 'Jankam31',
  port: 5432,
});

router.get('/users/all', [auth, permit("ROLE_ADMIN")], async (request, response) => {
  await pool.query(`SELECT id, name, username FROM users`, async (error, results) => {
    if (error) {
      throw error;
    }
    let users = [];
    for(let i = 0; i < results.rows.length; i++) {
      await pool
        .query(`SELECT * FROM user_roles WHERE user_id=${results.rows[i].id}`)
        .then(async result => {
          if(result.rows[0] && result.rows[0].role_id !== null) {
            await pool
              .query(`SELECT * FROM roles WHERE id = ${result.rows[0].role_id}`)
              .then((res) => {
                users = [...users, {...results.rows[i], role: res.rows[0].name}];
              })
          }
        });
    }
    return response.status(201).send(users);
  })
});

router.post('/adverts/activate', [auth, permit("ROLE_ADMIN")], async (request, response) => {
  const { id_advert, enabled } = request.body;
  await pool.query('SELECT * FROM public.app_advert WHERE id = $1',
    [id_advert], (error, results) => {
      if (error) {
        throw error;
      }
      console.log(results.rows);
      pool.query(
        'UPDATE public.app_advert SET enabled = $1 WHERE id = $2 RETURNING *',
        [enabled, results.rows[0].id],
        (error, result) => {
          if (error) {
            throw error
          }
          console.log(result.rows);
          return response.status(201).send({message: `Статус объявления успешно изменен`});
        }
      );
    });
});





module.exports = router;
