const Pool = require('pg').Pool;
const express = require('express');

const router = express.Router();

const pool = new Pool({
  user: 'postgres',
  host: '176.123.246.251',
  database: 'db_transportation',
  password: 'Jankam31',
  port: 5432,
});

router.get('/country/get/all', async (request, response) => {
  await pool.query('SELECT * FROM dict_country', async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

router.get('/city/get/byParent/:parentId', async (request, response) => {
  const country_id = request.params.parentId;
  await pool.query('SELECT * FROM dict_city WHERE country_id = $1', [country_id], async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

router.get('/carbrand/get/all', async (request, response) => {
  await pool.query('SELECT * FROM dict_car_brand', async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

router.get('/carmodel/get/byParent/:carModel', async (request, response) => {
  const parent_id = request.params.carModel;
  await pool.query('SELECT * FROM dict_car_model where parent_id = $1', [parent_id], async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

router.get('/cartype/get/all', async (request, response) => {
  await pool.query('SELECT * FROM dict_car_type', async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

router.get('/typeService/get/all', async (request, response) => {
  await pool.query('SELECT * FROM dict_type_service', async (error, results) => {
    if (error) {
      throw error;
    }
    return await response.status(201).json(results.rows);
  })
});

module.exports = router;
