const Pool = require('pg').Pool;
const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');

const config = require("../config");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathPlaceAdvert);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid(30) + "gg" + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const pool = new Pool({
  user: 'postgres',
  host: '176.123.246.251',
  database: 'db_transportation',
  password: 'Jankam31',
  port: 5432,
});

router.post('/advert', [auth, permit('ROLE_DRIVER', "ROLE_ADMIN"), upload.array('advertPhoto',7)], async (request, response) => {
  const { amount_payment, from_place, to_place, title, text, created_by_id, type_service_id, car_type_id, car_brand_id, car_model_id, carry_capacity, user_id} = request.body;

  let gallery = [];
  if (request.files) {
    gallery = request.files.map(file => file.filename)
  }
  pool
    .query(`SELECT * FROM cars`)
    .then(res => {
      let idCars = res.rows.length + 400;
      pool.query(`INSERT INTO cars (id, car_type_id, car_brand_id, car_model_id, user_id, carry_capacity) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
        [idCars++, car_type_id, car_brand_id, car_model_id, user_id, carry_capacity], (error, result) => {
          if (error) {
            throw error;
          }
          pool
            .query(`SELECT * FROM app_advert`)
            .then(res => {
              let idAdverts = res.rows.length + 1000;

              pool.query(`INSERT INTO app_advert (id, amount_payment, from_place, to_place, title, text, created_by_id, send_datetime, type_service_id, car_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *`,
                [idAdverts++, amount_payment, from_place, to_place, title, text, created_by_id, new Date(), type_service_id, result.rows[0].id], async (error, result) => {
                  if (error) {
                    throw error;
                  }
                  let advert = result.rows[0];
                  //app_advert_attachment file

                  for (let i = 0; i < gallery.length; i++) {
                    await pool
                      .query(`SELECT * FROM file`)
                      .then(async res => {
                        let idFiles = res.rows.length + gallery[i].length;
                        await pool.query(`INSERT INTO public.file (id, file_type, name) VALUES ($1, $2, $3) RETURNING *`, [idFiles + Math.floor(Math.random() * 1000000) + 1, 0, gallery[i]], async (error, res) => {
                          if (error) {
                            throw error;
                          }
                          let file = res.rows[0];
                          await pool
                            .query(`SELECT * FROM app_advert_attachment`)
                            .then(res => {
                              let idAppAdvert = res.rows.length + gallery[i].length;
                              pool.query(`INSERT INTO app_advert_attachment (id, fk_advert_id, fk_file_id) VALUES ($1, $2, $3) RETURNING *`, [idAppAdvert + Math.floor(Math.random() * 1000000) + 1, advert.id, file.id],async  (error, result) => {
                                return response.status(201).send({id: advert.id});
                              })
                            })
                        })
                      })
                  }

                })
            })
        })
    })

});

module.exports = router;
