const Pool = require('pg').Pool;
const express = require('express');

const auth = require('../middleware/auth');

const router = express.Router();

const pool = new Pool({
  user: 'postgres',
  host: '176.123.246.251',
  database: 'db_transportation',
  password: 'Jankam31',
  port: 5432,
});

router.get('/all', async (request, response) => {
  await pool.query(`SELECT * FROM app_advert LEFT JOIN public.users USING (id, enabled) ORDER BY username`, async (error, results) => {
    if (error) {
      throw error;
    }
    let adverts = [];
    let car = [];

    for (let i = 0; i < results.rows.length; i++) {
      await pool
        .query(`SELECT * FROM cars WHERE id=${results.rows[i].car_id}`)
        .then(async res => {
          let cars = res.rows[0];
          cars && cars.car_brand_id ?
          await pool
            .query(`SELECT * FROM dict_car_brand WHERE id=${cars.car_brand_id}`)
            .then(async res => {
              let cars_brand = res.rows[0];
              car = {cars_brand};
              await pool
                .query(`SELECT * from dict_car_model WHERE parent_id=${cars_brand.id}`)
                .then(res => {
                  let cars_model = res.rows[0];
                  car = {...car, cars_model};
                })
            }) : null;
          cars && cars.car_type_id ? await pool
            .query(`SELECT * FROM dict_car_type WHERE id=${cars.car_type_id}`)
            .then(res => {
              let car_type = res.rows[0];
              car = {...car, car_type}
            }) : null;
          await pool
            .query(`SELECT * FROM app_advert_attachment LEFT JOIN file ON file.id=fk_file_id WHERE fk_advert_id=${results.rows[i].id}`)
            .then(res => {
              const images = res.rows[0];
              adverts = [...adverts, {...results.rows[i], cars, images, car }];
            })
        })
    }

    return await response.status(201).json(adverts);
  })
});

router.get('/about', async (request, response) => {
  const advertId = request.query.advertId;
  await pool.query(`SELECT * FROM public.app_advert WHERE app_advert.id = ${advertId}`, async (error, results) => {
    if (error) {
      throw error;
    }
    let car = [];
    await pool
      .query(`SELECT * FROM cars WHERE id=${results.rows[0].car_id}`)
      .then(async res => {
        let carsModelQuery = res.rows[0];
        carsModelQuery && carsModelQuery.car_brand_id ?
          await pool
            .query(`SELECT * FROM dict_car_brand WHERE id=${carsModelQuery.car_brand_id}`)
            .then(async res => {
              let cars_brand = res.rows[0];
              car = {cars_brand};
              await pool
                .query(`SELECT * from dict_car_model WHERE parent_id=${cars_brand.id}`)
                .then(res => {
                  let cars_model = res.rows[0];
                  car = {...car, cars_model};
                })
            }) : null;
        carsModelQuery && carsModelQuery.car_type_id ? await pool
          .query(`SELECT * FROM dict_car_type WHERE id=${carsModelQuery.car_type_id}`)
          .then(res => {
            let car_type = res.rows[0];
            car = {...car, car_type}
          }) : null;
        await pool
          .query(`SELECT * FROM app_advert_attachment LEFT JOIN file ON file.id=fk_file_id WHERE fk_advert_id=${results.rows[0].id}`)
          .then(async res => {
            let images = null;
            if(res.rows.length !== 0) {
              images = res.rows;
            }
            await pool
              .query(`SELECT city_id, username, country_id FROM users WHERE id = ${results.rows[0].created_by_id}`)
              .then((res) => {
                return response.status(201).json({...results.rows[0], carsModelQuery, images, car, ...res.rows[0] });
              });
          })
      })
  })
});


router.delete('/delete_one/:advertId', [auth], async (request, response) => {
  const advertId = request.params.advertId;
  await pool.query(`DELETE FROM app_advert_attachment WHERE fk_advert_id = ${advertId}`);
  await pool.query(`DELETE FROM app_advert WHERE id = ${advertId}`);
  return response.status(201).json({message: "Объявление удалено"});
});


module.exports = router;
